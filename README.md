============================< Código >==================		


===========< Função Objetivo >=====			
			
GLPK.glp_set_obj_name(lp, "z");
            GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MAX);
            GLPK.glp_set_obj_coef(lp, 0, 0);
            GLPK.glp_set_obj_coef(lp, 1, fox1);//80
            GLPK.glp_set_obj_coef(lp, 2, fox2);//100
            GLPK.glp_set_obj_coef(lp, 3, fox3);//150
			
			onde: fox1, fox2, fox3 representam os coeficientes de cada x na FO

=================================< Restrição 1 >===============================================
GLPK.glp_set_row_name(lp, 1, "c1");
            GLPK.glp_set_row_bnds(lp, 1, GLPKConstants.GLP_UP, 0, r1Result);//100
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            GLPK.intArray_setitem(ind, 3, 3);
            GLPK.doubleArray_setitem(val, 1, r1x1);//5
            GLPK.doubleArray_setitem(val, 2, r1x2);//10
            GLPK.doubleArray_setitem(val, 3, r1x3);//20
            GLPK.glp_set_mat_row(lp, 1, 3, ind, val);
			
			onde: r1x1, r1x2, r1x3, r1Result dizem respeito aos coeficientes de x1, x2, x3 e o resultado  da restrição 1
				  GLPKConstants.GLP_UP nos diz o intervalo, nesse caso  -infinito <= restrição <= r1Result

			
=================================< Restrição 2 >===============================================

GLPK.glp_set_row_name(lp, 2, "c2");
            GLPK.glp_set_row_bnds(lp, 2, GLPKConstants.GLP_UP, 0, r2Result);
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            GLPK.intArray_setitem(ind, 3, 3);
            GLPK.doubleArray_setitem(val, 1, r2x1);//5
            GLPK.doubleArray_setitem(val, 2, r2x2);//10
            GLPK.doubleArray_setitem(val, 3, r2x3);//15
            GLPK.glp_set_mat_row(lp, 2, 3, ind, val);
			
			onde: r2x1, r2x2, r2x3, r2Result dizem respeito aos coeficientes de x1, x2, x3 e o resultado  da restrição 2
				  GLPKConstants.GLP_UP nos diz o intervalo, nesse caso  -infinito <= restrição <= r2Result
			
=================================< Restrição 3 >===============================================
			
GLPK.glp_set_row_name(lp, 3, "c3");
            GLPK.glp_set_row_bnds(lp, 3, GLPKConstants.GLP_UP, 0, r3Result);//1000
            GLPK.intArray_setitem(ind, 1, 2);
            GLPK.intArray_setitem(ind, 2, 3);
            GLPK.doubleArray_setitem(val, 1, r3x2);//5
            GLPK.doubleArray_setitem(val, 2, r3x3);//7
            GLPK.glp_set_mat_row(lp, 3, 2, ind, val);
			
			onde: r3x1, r3x2, r3x3, r3Result dizem respeito aos coeficientes de x1, x2, x3 e o resultado  da restrição 3
				  GLPKConstants.GLP_UP nos diz o intervalo, nesse caso  -infinito <= restrição <= r3Result
				  
				  Repare: Como na restrição 3 so tenho x2 e x3, meu vetor ind é setado q sua pos ind[1] = 2 e ind[2] = 3
									o que indica que vou trabalhar com os valores de x2 e x3
						  Ja preparado o vetor q me indica quais x vou usar, no vetor de valores (val) basta instanciar os 
									valores correspondentes a x2 e x3 logo val[1] = r3x2 e val[3] = r3x3


=================================< Definição das colunas >===============================================
			
GLPK.glp_add_cols(lp, 3);
            GLPK.glp_set_col_name(lp, 1, "x1");
            GLPK.glp_set_col_kind(lp, 1, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 1, GLPKConstants.GLP_LO, 0, x1);//0
            GLPK.glp_set_col_name(lp, 2, "x2");
            GLPK.glp_set_col_kind(lp, 2, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 2, GLPKConstants.GLP_LO, 0, x2);//0
            GLPK.glp_set_col_name(lp, 3, "x3");
            GLPK.glp_set_col_kind(lp, 3, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 3, GLPKConstants.GLP_LO, 0, x3);//0
			
			onde: em glp_set_col_kind defino o tipo que minha variavel será, no caso ela é  GLPKConstants.GLP_IV (ou seja inteira)
				  em glp_set_col_bnds é definido os limites das inha variaveis, nesse caso todas elas estão no intervalo
					0 <= x< +infinito. Isto é difinido com GLPKConstants.GLP_LO
			
=================================< Solção do problema Inteiro >===============================================

parm = new glp_iocp();
            GLPK.glp_init_iocp(parm);
            parm.setPresolve(GLPKConstants.GLP_ON);
            ret = GLPK.glp_intopt(lp, parm);
			
			onde: glp_init_iocp: inicia os parametros que me dizem tratar de um problema inteiro
				  parm.setPresolve: consegue tratar alguns problemas de redundância
				  glp_intopt resolve o problema construido.
		