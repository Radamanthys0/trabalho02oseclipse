package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
//import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_iocp;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;

/**
 * Servlet implementation class GlpkResult
 */
//@WebServlet("/GlpkResult")
@WebServlet(name = "GlpkResult", urlPatterns = {"/GlpkResult"})
public class GlpkResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	public glp_prob lp;

    public boolean bla;

    public double fox1;
    public double fox2;
    public double fox3;

    public double r1x1;
    public double r1x2;
    public double r1x3;
    public double r1Result;
//        double r1x1 = Double.parseDouble(request.getParameter("r1x1"));

    public double r2x1;
    public double r2x2;
    public double r2x3;
    public double r2Result;
//        double r1x1 = Double.parseDouble(request.getParameter("r1x1"));
    
    public double r3x2;
    public double r3x3;
    public double r3Result;

    public double x1;
    public double x2;
    public double x3; 
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//float total = Float.parseFloat(request.getParameter("total"));
		//System.out.println(total);
		//String maximizar = request.getParameter("maximizar");
		//request.setAttribute("maximizar", maximizar);
		
//		Minimize z = -.5 * x1 + .5 * x2 - x3 + 1 fox1
        //
        //	subject to
        //	0.0 <= x1 - .5 * x2 <= 0.2
        //  -x2 + x3 <= 0.4
        //	where,
        //	0.0 <= x1 <= 0.5
        //	0.0 <= x2 <= 0.5
        //      0.0 <= x3 <= 0.5
        fox1 = Double.parseDouble(request.getParameter("fox1"));
        fox2 = Double.parseDouble(request.getParameter("fox2"));
        fox3 = Double.parseDouble(request.getParameter("fox3"));
        System.out.println("FO -> z = " + fox1 + "x1" + fox2 + "x2" + fox3 + "x3");

        r1x1 = Double.parseDouble(request.getParameter("r1x1"));
        r1x2 = Double.parseDouble(request.getParameter("r1x2"));
        r1x3 = Double.parseDouble(request.getParameter("r1x3"));
        r1Result = Double.parseDouble(request.getParameter("r1Result"));
//        double r1x1 = Double.parseDouble(request.getParameter("r1x1"));
        System.out.println("r1 -> " + r1x1 + "x1" + r1x2 + "x2 <= " + r1Result);

        r2x1 = Double.parseDouble(request.getParameter("r2x1"));
        r2x2 = Double.parseDouble(request.getParameter("r2x2"));
        r2x3 = Double.parseDouble(request.getParameter("r2x3"));
        r2Result = Double.parseDouble(request.getParameter("r2Result"));
//        double r1x1 = Double.parseDouble(request.getParameter("r1x1"));
        System.out.println("r2 -> " + r2x2 + "x2" + r2x3 + "x3 <= " + r2Result);
        
        
        r3x2 = Double.parseDouble(request.getParameter("r3x2"));
        r3x3 = Double.parseDouble(request.getParameter("r3x3"));
        r3Result = Double.parseDouble(request.getParameter("r3Result"));

        x1 = Double.parseDouble(request.getParameter("x1"));
        System.out.println("x1 <= " + x1);
        x2 = Double.parseDouble(request.getParameter("x2"));
        System.out.println("x2 <= " + x2);
        x3 = Double.parseDouble(request.getParameter("x3"));
        System.out.println("x3 <= " + x3);

        int ret = solveProblem();

        if (ret == 0) {
            write_lp_solution(lp ,request);
        } else {
            System.out.println("The problem could not be solved");
        }
		
		
		request.getRequestDispatcher("glpResult.jsp").forward(request, response);
	}
	
	
	public int solveProblem() {
		glp_iocp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret = 0;

        try {
            // Create problem
            lp = GLPK.glp_create_prob();
            System.out.println("Problem created");
            GLPK.glp_set_prob_name(lp, "myProblem");

         // Define columns
            GLPK.glp_add_cols(lp, 3);
            GLPK.glp_set_col_name(lp, 1, "x1");
            GLPK.glp_set_col_kind(lp, 1, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 1, GLPKConstants.GLP_LO, 0, x1);//0
            GLPK.glp_set_col_name(lp, 2, "x2");
            GLPK.glp_set_col_kind(lp, 2, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 2, GLPKConstants.GLP_LO, 0, x2);//0
            GLPK.glp_set_col_name(lp, 3, "x3");
            GLPK.glp_set_col_kind(lp, 3, GLPKConstants.GLP_IV);
            GLPK.glp_set_col_bnds(lp, 3, GLPKConstants.GLP_LO, 0, x3);//0
            
            // Create constraints
            // Allocate memory
            ind = GLPK.new_intArray(4);
            val = GLPK.new_doubleArray(4);// AUMENTEI

            // Create rows
            GLPK.glp_add_rows(lp, 3);

//             5x1 + 10x2 + 20x3  >= 100
//             5x1 + 10x2 + 15x3 >= 1000
//            5x2 + 7x3 >= 1000
            
            // Set row details
            GLPK.glp_set_row_name(lp, 1, "c1");
            GLPK.glp_set_row_bnds(lp, 1, GLPKConstants.GLP_UP, 0, r1Result);//100
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            GLPK.intArray_setitem(ind, 3, 3);
            GLPK.doubleArray_setitem(val, 1, r1x1);//5
            GLPK.doubleArray_setitem(val, 2, r1x2);//10
            GLPK.doubleArray_setitem(val, 3, r1x3);//20
            GLPK.glp_set_mat_row(lp, 1, 3, ind, val);

            GLPK.glp_set_row_name(lp, 2, "c2");
            GLPK.glp_set_row_bnds(lp, 2, GLPKConstants.GLP_UP, 0, r2Result);
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            GLPK.intArray_setitem(ind, 3, 3);
            GLPK.doubleArray_setitem(val, 1, r2x1);//5
            GLPK.doubleArray_setitem(val, 2, r2x2);//10
            GLPK.doubleArray_setitem(val, 3, r2x3);//15
            GLPK.glp_set_mat_row(lp, 2, 3, ind, val);

            GLPK.glp_set_row_name(lp, 3, "c3");
            GLPK.glp_set_row_bnds(lp, 3, GLPKConstants.GLP_UP, 0, r3Result);//1000
            GLPK.intArray_setitem(ind, 1, 2);
            GLPK.intArray_setitem(ind, 2, 3);
            GLPK.doubleArray_setitem(val, 1, r3x2);//5
            GLPK.doubleArray_setitem(val, 2, r3x3);//7
            GLPK.glp_set_mat_row(lp, 3, 2, ind, val);
            
            
            // Free memory
            GLPK.delete_intArray(ind);
            GLPK.delete_doubleArray(val);

            // Define objective         //	Minimize z = -.5 * x1 + .5 * x2 - x3 + 1 fox1
            GLPK.glp_set_obj_name(lp, "z");
            GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MAX);
            GLPK.glp_set_obj_coef(lp, 0, 0);
            GLPK.glp_set_obj_coef(lp, 1, fox1);//80
            GLPK.glp_set_obj_coef(lp, 2, fox2);//100
            GLPK.glp_set_obj_coef(lp, 3, fox3);//150

            // Write model to file
            // GLPK.glp_write_lp(lp, null, "lp.lp");
            // Solve model
//            parm = new glp_smcp();
//            GLPK.glp_init_smcp(parm);
//            ret = GLPK.glp_simplex(lp, parm);//MUDAR PARA INTEIRO
//            
            parm = new glp_iocp();
            GLPK.glp_init_iocp(parm);
            parm.setPresolve(GLPKConstants.GLP_ON);
            ret = GLPK.glp_intopt(lp, parm);


            // Retrieve solution
//            if (ret == 0) {
//                write_lp_solution(lp);
//            } else {
//                System.out.println("The problem could not be solved");
//            }
            
            // Free memory
            GLPK.glp_delete_prob(lp);
            
        } catch (Exception ex) {//GlpkException ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    static void write_lp_solution(glp_prob lp, HttpServletRequest request) {
        int i;
        int n;
        String name;
        double val;

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_mip_obj_val(lp);
        System.out.print(name);
        System.out.print(" = ");
        System.out.println(val);
        request.setAttribute("z", val);
        n = GLPK.glp_get_num_cols(lp);
        for (i = 1; i <= n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_mip_col_val(lp, i);
            System.out.print(name);
            System.out.print(" = ");
            System.out.println(val);
            request.setAttribute("respx"+i, val);
        }
        
    }

}
