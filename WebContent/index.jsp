<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pacotes de Internet</title>
</head>
    <body>
    
  <nav class="blue darken-4">
    <div class="nav-wrapper customMargin">
      <a href="#" class="brand-logo center">Pacotes de internet</a>
    </div>
  </nav>
      
        <div class="container">
            <form action="GlpkResult" method="POST">
                <div class="row">
                    <div class="input-field col s2">
          				<label class="black-text" >Maximizar Lucro</label>
  			 		</div>
                    <div class="input-field col s2">
                            <input id="fox11" type="text" class="validate" name="fox1">
          					<label for="fox11" class="black-text">Pacote 1</label>
                    </div>
                    <div class="input-field col s2">
                           <input id="fox22" type="text" class="validate" name="fox2">
          					<label for="fox22" class="black-text">Pacote 2</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="fox33" type="text" class="validate" name="fox3">
          					<label for="fox33" class="black-text">Pacote 3</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s2">
          				<label class="black-text" >Link:</label>
  			 		</div>
  			 		<div class="input-field col s2">
                            <input id="r1x1" type="text" class="validate" name="r1x1">
          					<label for="r1x1" class="black-text">Pacote 1</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r1x2" type="text" class="validate" name="r1x2">
          					<label for="r1x2" class="black-text">Pacote 2</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r1x3" type="text" class="validate" name="r1x3">
          					<label for="r1x3" class="black-text">Pacote 3</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r1Result" type="text" class="validate" name="r1Result">
          					<label for="r1Result" class="black-text">Disponibilidade(<=)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s2">
          				<label class="black-text" >Fio Comum:</label>
  			 		</div>
  			 		<div class="input-field col s2">
                            <input id="r2x1" type="text" class="validate" name="r2x1">
          					<label for="r2x1" class="black-text">Pacote 1</label>
                    </div>
  			 		<div class="input-field col s2">
                            <input id="r2x2" type="text" class="validate" name="r2x2">
          					<label for="r2x2" class="black-text">Pacote 2</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r2x3" type="text" class="validate" name="r2x3">
          					<label for="r2x3" class="black-text">Pacote 3</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r2Result" type="text" class="validate" name="r2Result">
          					<label for="r2Result" class="black-text">Disponibilidade(<=)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s2">
          				<label class="black-text" >Fibra Ótica:</label>
  			 		</div>
  			 		<div class="input-field col s2">
                            <input id="r3x2" type="text" class="validate" name="r3x2">
          					<label for="r3x2" class="black-text">Pacote 2</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r3x3" type="text" class="validate" name="r3x3">
          					<label for="r3x3" class="black-text">Pacote 3</label>
                    </div>
                    <div class="input-field col s2">
                            <input id="r3Result" type="text" class="validate" name="r3Result">
          					<label for="r3Result" class="black-text">Disponibilidade(<=)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s2">
          				<label class="black-text" >Onde:</label>
  			 		</div>
  			 		<div class="input-field col s2">
                            <input id="x1" type="text" class="validate" name="x1">
          					<label for="x1" class="black-text">x1 >= </label>
                    </div>
                    <div class="input-field col s2">
                            <input id="x2" type="text" class="validate" name="x2">
          					<label for="x2" class="black-text">x2 >= </label>
                    </div>
                    <div class="input-field col s2">
                            <input id="x3" type="text" class="validate" name="x3">
          					<label for="x3" class="black-text">x3 >= </label>
                    </div>
                </div>
                <div class="col s6 offset-s6">
               		 <input type="submit" class="btn btn-success" value="Calcular">
                </div>
                
            </form>
        </div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </body>
</html>