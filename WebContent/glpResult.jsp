<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pacotes de internet</title>
</head>
<body>

<nav class="blue darken-4">
    <div class="nav-wrapper customMargin">
      <a href="#" class="brand-logo center">Pacotes de internet</a>
    </div>
  </nav>
  
  <div class="container">
  	<div class="row">
        <h4>Resultado GLPK</h4>
     </div>
     <div class="row">
     	<div class="input-field col s2">			
			<label class="black-text">Lucro = </label>
		</div>
		<div class="input-field col s6">	
			<input disabled  type="text" size="10" value="<%= request.getAttribute("z")%>"><br>
		</div>
	</div>
	<div class="row">
	<div class="col s2">		
        <label class="black-text">Pacote 1 = </label>
        </div>
        <div class="input-field col s10">
        <input disabled  type="text" size="10" value="<%= request.getAttribute("respx1")%>"><br>
        </div>
    </div>
    <div class="row">
    	<div class="col s2">	
        <label class="black-text">Pacote 2 = </label>
        </div>
        <div class="input-field col s10">
        <input disabled  type="text" size="10" value="<%= request.getAttribute("respx2")%>"><br>
        </div>
    </div>
    <div class="row">
    <div class="col s2">	    
        <label class="black-text">Pacote 3 = </label>
        </div>
        <div class="input-field col s10">
        <input disabled  type="text" size="10" value="<%= request.getAttribute("respx3")%>"><br>
        </div>
    </div>
        
     <div class="col s4 offset-s8">
        <input type="button" class="btn btn-success" value="retornar" onclick="history.back()">
     </div>
        
 </div>
        
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </body>
</html>